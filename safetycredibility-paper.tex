% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04
%
\documentclass[runningheads]{llncs}
%
\usepackage{graphicx}
\graphicspath{{figures/}}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:

\usepackage{wrapfig}

\usepackage{hyperref}
\hypersetup{hidelinks,
  colorlinks=true,
  allcolors=black,
  pdfstartview=Fit,
  breaklinks=true}

\usepackage{url}
\renewcommand\UrlFont{\color{blue}\rmfamily}

\usepackage{microtype}
% \usepackage{floatflt}

\usepackage{cleveref}
\usepackage{paralist}
\usepackage[nolist]{acronym}
\newacro{RoSAS}[RoSAS]{Robotic Social Attributes Scale}
\newacro{fetch}[\emph{Fetch}]{\emph{Fetch Mobile Manipulator}}
\newacro{HRI}[HRI]{Human-Robot Interaction}
\newacro{AN}[\emph{AN}]{\emph{According to social Norms}}
\newacro{VN}[\emph{VN}]{\emph{Violating social Norms}}
\newacro{oven}[\emph{oven}]{\emph{severe}}
\newacro{plugs}[\emph{power plugs}]{\emph{minor}}
\newacro{pepper}[\emph{Pepper}]{\emph{moderate}}

\begin{document}

%
\title{How a Robot's Social Credibility Affects Safety Performance\thanks{Supported by the Assuring Autonomy International Programme and the University of Hertfordshire's Robot House}}
%
%\titlerunning{Abbreviated paper title}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
\author{Patrick Holthaus\inst{1}\orcidID{0000-0001-8450-9362} \and
Catherine Menon\inst{1}\orcidID{0000-0003-2072-5845} \and
Farshid Amirabdollahian\inst{1}\orcidID{0000-0001-7007-2227}}
%
\authorrunning{P. Holthaus et al.}

\institute{Adaptive Systems Research Group, School of Engineering and Computer Science, University of Hertfordshire, Hatfield AL10 9AB, United Kingdom\\
\email{\{p.holthaus,c.menon,f.amirabdollahian2\}@herts.ac.uk}}


\maketitle

\begin{abstract}
This paper connects the two domains of \ac{HRI} and safety engineering to ensure that the design of interactive robots considers the effect of social behaviours on safety functionality.
We conducted a preliminary user study with a social robot that alerts participants during a puzzle-solving task to a safety hazard.
Our study findings show an indicative trend where users who were interrupted by a socially credible robot were more likely to act to mitigate the hazard than users interrupted by a robot lacking social credibility.

\keywords{Human-Robot interaction  \and Social credibility \and Robot safety.}
\end{abstract}

\section{Introduction}\label{sec:introduction}

% Context: Social robotics
In \ac{HRI}, the social capabilities of interactive robots are of primary interest due to their impact on acceptability.
Social capabilities such as proxemics, gestures, head orientation and gaze direction have been shown to improve a robot's interaction quality \cite{Bensch2017}, to lead to a better user understanding of their behaviours \cite{Lichtenthaler2016}, and to facilitate the ways in which robots can learn from humans \cite{Breazeal2009}.
Similarly, lack of appropriate social capabilities can cause end-users to resist or minimise engagement with the robot \cite{Klamer2010}.
An ongoing research challenge in this area is to identify appropriate domain-specific social behaviours that meet the user expectations for a robot. % TODO citation! there's something on expectations by Leite, Lohse etc
Such social behaviours need to be consistent with the environment, the robot's other functionality, and the intended course of the interaction. % TODO citation! there's something in my thesis but probably more somewhere
We refer to robots which demonstrate a well integrated compilation of such social behaviours as being socially \emph{credible} \cite{Menon2019}.

% Context: Safety
As well as social behaviours, another area of concern is the safety performance of interactive robots.
Within the UK, the Health and Safety Executive requires that the risk posed by such systems should be reduced ``As Low As Reasonably Practicable'' \cite{HSE}.
This requires a consideration of the risks that an interactive robot might pose, as well as functions that it can perform safely \cite{ISO13482}.
In a domestic context, one of the primary safety functions performed by an interactive assistive robot is alerting the user to potential hazards.
For example, the robot might remind the user to take necessary medication if this is overdue, or alert the user to an oven which has been left on.
In this way, the robot and human act together to mitigate hazards which arise externally.
Identifying safety functionality is an ongoing research area in robotics, as is identification of the ways in which robots might mitigate hazards present in the environment.

%
% Why is the topic insteresting to the discipline?
Our work brings together the two domains of \ac{HRI} and safety engineering to ensure that the design of interactive robots considers both safety functionality and social behaviours.
Both of these must be \emph{designed into} the robot, and safety and social requirements can interact in complex ways.

% What is the current debate (does it exist)?
% Yet, little research on the interplay of both topics has been undertaken.
Current assistive robots are being designed with both safety and social considerations in mind.
For example, the Care-O-Bot 4 has been constructed to take account of both end-user acceptability and safety considerations \cite{Care-Data-Sheet}.
In addition, international safety standards such as ISO 13482 \cite{ISO13482} consider the hazards presented by such robots when undertaking their specified behaviour (which may include social actions).
However, owing to the comparative rarity and novelty of domestic interactive robots, there has not yet been significant academic work looking specifically at how safety and social behaviours might interact with each other.
Safety standards promote a design-for-safety approach which, if considered in isolation, may negatively influence the possible social behaviours which the robot can demonstrate.
Similarly, designing for social behaviours alone may not adequately consider the safety requirements of these robots and the behaviours needed to fulfill them.


% What is the statement/hypothesis?
It is our position that both safety and social considerations can have an effect on each other, with the social behaviours of the robot affecting its safety performance and vice versa \cite{Menon2019}.
In this paper, we seek to identify a connection between a robot's social actions and the  effectiveness of its safety performance.
Specifically, we hypothesise that social deficiencies in a robot can undermine its perceived authority when alerting a user to hazards in the domestic environment, and consequently reduce its fitness to serve as a safety monitoring device in the home.
%
% Structure of the argument
As a first approach, we present a preliminary study that investigates user responses when notified of a safety hazard by either a socially credible robot, or a robot that lacks social credibility.
The study aims to provide an initial link between a robot's social credibility and user willingness to act on its safety-related alert functions.
%
% Contribution
Our study findings show an indicative trend that users who were alerted to an environmental hazard by a socially credible robot are more likely to act on this alert and mitigate the hazard than users alerted to it by a robot lacking social credibility.

% TODO probably okay to leave out in case space is needed - which probably is
% \Cref{sec:related} establishes the context for our work by discussing relevant related work from both disciplines and highlighting connecting phenomena. The experimental design and procedure of the conductetd study is presented in \Cref{sec:method}. Questionnaire feedback and metrics on user behaviour are presented in \Cref{sec:results}. We discuss these results in \Cref{sec:discussion} and identify implications on both scientific fields before concluding the paper in \Cref{sec:conclusion} and indicating further research.

\section{Related Work}\label{sec:related}

%TODO these are just pointers, fill with actual text

This section introduces concepts we rely on and explore in the conducted experiment. We discuss socially appropriate and credible behaviours as well as safety in \ac{HRI} and related domains. Existing research that connects both areas has historically been limited to the phenomenon of trust in social robots. \cite{vandenBrule2014} explores the relation of robot performance and behavioural style on a user's trust. Likewise, \cite{Salem2015} shows that the task-type has an influence on whether a person follows robot orders. Standards such as ISO 13482 \cite{ISO13482} do consider the need to build safety into social robots from the beginning, but are still relatively new.



\subsection{Socially appropriate behaviours}\label{sec:related:social}

Interactive robots are often equipped with social behaviours to improve various performance aspects in their respective domain \cite{Breazeal2016}.
Verbal and non-verbal behaviours can have a positive effect on people's perceived interaction quality when they are easy to comprehend \cite{Lichtenthaler2016} and meet expectations \cite{Bartneck2004}.

People have, for example, personal preferences when asked about comfortable interaction distances and angles \cite{Syrdal2007}.
Yet, they apply similar metrics to robots as to humans \cite{Koay2014}.
It has been further shown that human-like body orientations before and during face-to-face interaction have a positive effect on the perceived interaction quality \cite{Holthaus2011}.
Robot navigation that respects human personal space \cite{Koay2017} and employs appropriate passing distances is also believed to increase the acceptance of robots in the home and public \cite{Rios-Martinez2015}.
Head orientation and gaze can be used to demonstrate a robot's current focus of attention \cite{Renner2014} and facilitate social bonds with humans \cite{Admoni2017}.
Differences in robot politeness are easily detected by humans but are not necessarily influencing the interaction quality \cite{Salem2013}.
However, a positive effect of polite utterances on user engagement can be identified \cite{Castro2016}.

\subsection{Safety and human interactions}\label{sec:related:safety}
Although not looking specifically at interactive robots, much work exists on the efficacy of safety-critical (partially) autonomous systems which alert users to hazards or act in coordination with users to mitigate hazards. Such systems include sat-navs, speed monitoring systems, cockpit monitoring systems, automated vehicle operational alerts and medical devices. For all of these systems a known risk is user disengagement: if the user ceases to engage with or pay attention to the system, they are unable to mitigate hazards identified by the system.

Studies have shown that the method of alert or interruption used by the system has significant implications for user disengagement. In \cite{Wall2013}, users chose to switch off a speed monitoring and sat-nav system that they found "irritating", even while acknowledging that the speed warnings improved safety. Still in the automotive domain, there have been a number of accidents involving automated vehicles which are in part due to user disengagement with the systems. \cite{NTSB} discusses the case of a Tesla crash in automated mode, in which the user had failed to engage with the system despite repeated warnings and \cite{NTSB2} discusses a similar case, where repeated warnings were ignored. In \cite{Sarter1997}, it was found that when a cockpit alert was given, pilots would attempt to debug the automation instead of acting on the alert. This was attributed to the fact that the pilots were monitoring status via the flight control unit (which shows commanded paths, rather than actual) instead of the automation. The alert contradicted their perspective of the system, and was judged to be a failure of automation.



\section{Method}\label{sec:method}


We conducted a preliminary study with 30 participants that investigates their responses when notified of different safety hazards by either a socially credible robot, or a robot that explicitly violates these social norms.
The study aims to establish a link between a robot's social credibility and its authority regarding safety-related functions.
We thereby hypothesise a negative effect of a lack of social credibility on the willingness of humans to follow safety-related alerts and the thoroughness of their actions.
The experiment was approved by the University of Hertfordshire's Health, Science, Engineering and Technology Ethics Committee under protocol number COM/SF/UH/03714.

\subsection{Experimental design}\label{sec:method:design}

We manipulated two independent variables of which one varied in two and the other in three dimensions so that we ended up with a 2x3 experiment design. As we assume an effect of the manipulation on thoroughness regarding safety warnings given by the robot, we used a combination of questionnaires as well as response types and timings to hazard warnings as measurements.

\subsubsection{Independent variables}
The first variable describes the character of the robot's social behaviour, which was either \ac{AN} as described in \Cref{sec:related:social} or \ac{VN}. This variable is designed as two conditions between subjects (15 participants per condition). In total, we altered the robot behaviour in the following characteristics:
\begin{inparaenum}[a)]
 \item its distance during greeting (appropriate vs too far)
 \item its passing distance during puzzle solving (appropriate vs too close)
 \item its position during interruption (frontal vs from behind)
 \item its head position during interruption (up vs down)
 \item its verbal interruptions and confirmations (impolite vs polite, cf.~\Cref{tab:utterances}).
\end{inparaenum}
All other behaviour, including verbal hazard alerts were unanimous in both conditions to ensure a proper understanding of the alert.

\begin{table}
\def\arraystretch{1.2}
\caption{Robot utterances by condition}
\begin{tabular}{lll}
\textbf{Utterance} & \textbf{According (AN)}               & \textbf{Violating (VN)}\\\hline
Interrupt          & Excuse me?                            & Hey!\\
Greet              & Hello and welcome to Robot House.     & Another one, then.\\
Begin              & Please sit down and begin your        & You can sit down and\\
                   & puzzle now.                           & begin your puzzle now.\\
Action             & Thank you.                            & Good.\\
No Action          & -                                     & Good.\\
Ending             & Please wait for the experimenter now. & They are coming to see you now.
\end{tabular}
\label{tab:utterances}
\end{table}


\begin{figure}[t]
  \includegraphics[height=.1815\textheight]{kitchen2}
  \includegraphics[height=.1815\textheight]{kitchen1}
  \includegraphics[height=.1815\textheight]{pepper2}
  \caption{Participant manipulation areas. The \ac{oven}, switchable \ac{plugs} with appliances and the \ac{pepper} with its information display are depicted left to right.}
  \label{fig:manipulations}
\end{figure}

We further assumed a difference in the perceived severity of various hazards (cf.~\Cref{sec:related:safety}). Accordingly, we introduced a second independent variable as three successive experiment phases, with each participant experiencing all phases. During each phase (cf.~\Cref{sec:method:procedure}), the robot notified the participant of one of the following safety hazards:
\begin{inparaenum}
 \item[(\acl{oven})] The \acs{oven} in the kitchen has been left on;
 \item[(\acl{plugs})] Some \acs{plugs} in the kitchen have been left on; and
 \item[(\acl{pepper})] The \acs{pepper} robot in the bedroom is overheating.
\end{inparaenum}
\Cref{fig:manipulations} depicts the three manipulation areas for the participants.

\subsubsection{Dependent variables}
We measured the following dependent variables for each participant to investigate our hypothesis:
\begin{inparaenum}[(1)]
 \item their assessment of severity for each hazard
 \item their perception of the robot as a social agent
 \item their willingness and thoroughness to react to robot warnings.
\end{inparaenum}


% To confirm the manipulation of the robot's social behaviour, we asked participants to evaluate the robot using questionnaires

% \subsubsection{Questionnaire}
The questionnaire
%\footnote{Questionnaire available at \url{https://gitlab.com/pholthau/latex-questionnaire/-/jobs/artifacts/SocCred/raw/My-Questionnaire.pdf?job=PDFs}}
that was given to each participant after the experiment was composed of multiple parts:
Firstly, demographic information of age, gender and prior experience with robots was gathered.
Following were semantic-differential questions (safe-dangerous) that assessed the physical safety of the three different hazards \ac{oven}, \ac{plugs}, or \ac{pepper} overheating to measure the first dependent variable (1).
We then included two questionnaires to verify our experimental conditions and investigate dependent variable (2): The \ac{RoSAS} \cite{Carpinella2017} and the \emph{GodSpeed} questionnaire \cite{Bartneck2009} followed by individual Likert-style questions \cite{Likert1932} about the robot's sociability.
We added further Likert-style questions to verify people attributed the robot an understanding of safety-relevant situations.
Lastly, we asked participants open questions about reasons why they decided to act or not to act to investigate variable (3).

% \subsubsection{Other measurements}
Besides using subjective questionnaires, we measured dependent variable (3) with objective criteria, i.e.
\begin{inparaenum}[(I)]
 \item whether participants actively responded to the utterance by standing up and
 \item the amount of time spent to perform an action that eliminates the hazard.
\end{inparaenum}
Criterium (I) was observed and annotated using the live video feed while (II) was measured using the robot house sensory infrastructure.
In case of the \ac{oven}, no timings were available.
The \ac{plugs} timings were measured four times (cf.~\Cref{sec:method:procedure}) and averaged.

\subsection{Experimental procedure}\label{sec:method:procedure}

\subsubsection{Environment}

The study was carried out in the Robot House, a four-bedroom home near the university campus used for human-robot experiments.
Besides standard furniture and appliances found in a typical house, it is also equipped with smart home sensors and actuators.
In the current experiment, we used an omni-directional ceiling camera to monitor and record the interaction.
We also manipulated and recorded the kitchen's power plugs to measure participants' responses to robot prompts.
As the main interaction medium we opted to use a \ac{fetch} robot \cite{Wise2016}. % TODO WHY? Not really social!
In addition, \emph{Pepper} \cite{Pepper} serves as a secondary robot that poses a potential safety hazard (cf.~\Cref{sec:method:design}). It remains non-interactive in a standing posture throughout the experiment. It displays pseudo sensory data and a shutdown button that triggers a resting position on its screen.
We tasked the participants with doing cognitive puzzles (Sudoku) to keep them occupied and give them a valid reason for ignoring the robot.


% Image: Kitchen appliances
% Image: Robot in bedroom
% Image: Floor plan

\subsubsection{Course of Action}

\begin{wrapfigure}{l}{.55\textwidth}
\vspace*{-.8cm}
\centering
\includegraphics[width=.55\textwidth]{robothouse-map}
\caption{Overview of experimentation area. Participants solved the task and filled out the questionnaire at the position of the green circle. The (initial) robot positions are indicated with blue squares. Brown crosses indicate searching locations for the robot. The \ac{oven} and switchable \ac{plugs} are also marked inside the kitchen.}
\label{fig:map}
\vspace*{-.8cm}
\end{wrapfigure}

Participants were given the information sheet and  were asked if they had any queries or questions. They could begin the experiment upon consent to participate.

We then introduced each participant to the house (cf.~\Cref{fig:map}) starting with the kitchen.
We pointed them towards all visible appliances, mentioning the oven and switchable power sockets amongst others.
We explained that the robot has remote access to the sensors and knows about their state, for example whether the oven and power sockets are on.
We also pointed at the \ac{pepper} robot in the bedroom from afar and mentioned that it was plugged in and currently charging.

Following this, we showed participants the \ac{fetch} robot that would be used for the experiment, and ran through basic safety information.
We explained that the participant should sit at the living room table, completing as many cognitive tasks (i.e., Sudoku puzzles) as possible in the allotted time.
We told them that the robot may interrupt them at times, and that should they wish to, they may choose to perform an action in response to this interruption. We told them that it was up to them to decide whether to perform an action or not, but that if they did perform an action, then when this was completed they should return to their position sitting at the table again.

We then left the room and the \ac{fetch} robot performed its initial greeting behaviour for the participant using the utterance according to the current condition (cf.~\Cref{tab:utterances}). The robot told the participant to sit and begin with their Sudoku puzzles, and then followed the procedure below for each phase (\ac{oven}, \ac{plugs} x2, \ac{pepper}):
\begin{inparaenum}

\end{inparaenum}


\begin{enumerate}
  \item Go to parking position in the living room and wait 30 seconds.
  \item Inspect the area by turning around and moving the camera head around.
  \item Navigate to the location in which the robot finds a possible threat (kitchen in case of \ac{oven} and \ac{plugs}, looking towards bedroom in case of \ac{pepper})
  \item Inspect the area with the head again.
  \item Navigate to participant, interrupt and alert them about the item in question.
\end{enumerate}

As part of its behaviour the robot navigates to an alternative position behind the participant before the second interruption (\ac{plugs} 1). We do this for two reasons: Firstly, all participants experience a robot that moves behind their back. Secondly, the robot has to pass the participants with a certain distance which contributes to the manipulation of the independent variable.

After the participant has taken any actions they choose (including ignoring the robot or leaving the room to switch the oven off and returning), the experimenter will trigger a condition- and action-dependent acknowledgement (cf.~\Cref{tab:utterances}) and the next phase begins.
%
After the last iteration, the robot will ask the participant to wait for the experimenter.
The questionnaires were then given to each participant.
Finally, we asked the participant if they have any questions for us, thanked them for their time, and helped them leave.
%
At least one experimenter was in the house at all times, to monitor the robot, the switched-on oven and the wall socket power switches with the help of cameras and power consumption sensors.

\section{Results}\label{sec:results}

\begin{wrapfigure}{r}{.25\textwidth}
\vspace*{-2.2cm}
\centering
\includegraphics[width=.25\textwidth,page=1]{Rplots}%
\caption{Safety ratings per condition (\ac{AN} left, \ac{VN} right) for every warning. The boxes display scores on a 5-point scale (1: safe; 5: dangerous) for each device (\ac{oven}, \ac{plugs}, \ac{pepper}).}
\label{fig:severity}
\vspace*{-2.8cm}
\end{wrapfigure}
% \fbox{may be you can add legend to figure 3, as left and right is not accurate to address 6 boxes}

As this study was a preliminary study, and thus low participant numbers, no statistical significance between conditions was expected for the evaluation of the questionnaires. In this section, we instead report tendencies and trends we can identify in the collected data.
%TODO more explanation: we still did an analysis but nothing came up

\subsubsection{Participant's assessment of severity}



While there is no apparent difference between the two social conditions, variances for \ac{plugs} are higher for \ac{AN} and variances for \ac{pepper} are higher for \ac{VN}. Across conditions, ratings regarding the danger differ between the types of hazards, see \Cref{fig:severity}.
Distinct differences are noticable between \ac{oven} and \ac{plugs}. Potential danger is also rated high for \ac{pepper}.
% TODO add numbers
%Oven \ac{AN}: 4.71 (0.47) \ac{VN}: 4.12 (1.59)
%Plugs \ac{AN}: 2.21 (1.12) \ac{VN}: 1.81 (1.22)
%Robot \ac{AN}: 4.21 (0.58) \ac{VN}: 3.94 (1.12)

\subsubsection{Perception of the robot as a social agent}
Although we do not observe significant differences between the two social conditions, tendencies indicate that \ac{AN} is performing better in every item of the \emph{GodSpeed} and \ac{RoSAS} questionnaire. The following mean values and standard deviation have been recorded: Anthropomorphism \ac{AN}: 3 (0.87) \ac{VN}: 2.75 (0.64), Animacy \ac{AN}: 3.23 (0.78) \ac{VN}: 2.98 (0.77), Likeability \ac{AN}: 3.96 (0.55) \ac{VN}: 3.58 (0.67), Perceived Intelligence \ac{AN}: 4.23 (0.45) \ac{VN}: 3.96 (0.46), Warmth \ac{AN}: 2.7 (1.07) \ac{VN}: 2.79 (1.22), Competence \ac{AN}: 4.57 (1.02) \ac{VN}: 4.34 (1.22), Discomfort \ac{AN}: 1.8 (0.75) \ac{VN}: 2.29 (0.97).


\subsubsection{Willingness and thoroughness to react to robot warnings}

\begin{figure}[t]
\centering
\includegraphics[width=.25\textwidth,page=8]{Rplots}%
\includegraphics[width=.25\textwidth,page=9]{Rplots}%
\includegraphics[width=.25\textwidth,page=10]{Rplots}%
\includegraphics[width=.25\textwidth,page=11]{Rplots}%
\caption{Participant responses to hazard warnings. The bars depict absolute response rates for each warning (\ac{oven}, \ac{plugs} x2, \ac{pepper}) grouped by condition (\ac{AN}/\ac{VN}). The first column gives the number of people who responded [R] to the utterance by standing up and exploring the area. The second column denotes whether participants carried out the intended physical manipulation [M] at the target object.}
\label{fig:responses}
\end{figure}



\begin{wrapfigure}{r}{.25\textwidth}
\vspace*{-.9cm}
\centering
\includegraphics[width=.25\textwidth,page=12]{Rplots}%
\caption{Manipulation times per condition (\ac{AN} left, \ac{VN} right) for each hazard warning. The boxes display the time in seconds people needed to shut off a device (\ac{plugs} x2 and \ac{pepper}) if they manipulated the object.}
\label{fig:timings}
\vspace*{-2.7cm}
\end{wrapfigure}

Results show a general tendency for people in the \ac{AN} condition to respond better to the robot's hazard warnings (cf.~\Cref{fig:responses}), in particular at the second \ac{plugs} phase.
Furthermore, there is a noticeable decline in response rate compared to the first \ac{plugs} warning.
In the \ac{AN} condition it seems that participants almost always performed an action whenever they showed a reaction as opposed to the \ac{VN} condition where they sometimes reacted but decided against the manipulation of objects.
Participants in the \ac{VN} condition additionally took longer on average before switching off any \ac{plugs} if they did (cf.~\Cref{fig:timings}).
\ac{pepper} was switched off after approximately the same time period.


% \begin{itemize}
% \item Responded better with \ac{AN} than \ac{VN} (in general) - in particular with \ac{plugs} case \Cref{fig:responses}
% \item Response rate declined for \ac{VN} for \ac{plugs} 2 \Cref{fig:responses}
% \item \ac{VN} took longer to turn Pepper off if they did at all %TODO - PH to check statistics - CM Doesn't seem to be confirmed - see figure!
% \end{itemize}

% \begin{figure}[t]
% \centering
% \includegraphics[width=.4\textwidth,page=3]{Rplots}%
% \includegraphics[width=.4\textwidth,page=4]{Rplots}%
% \caption{Social attribute questionnaire scores per condition (\ac{AN} left, \ac{VN} right). The boxes display average ratings for each of the questionnaire's items. \emph{GodSpeed} scores were given on a 5-point scale, \ac{RoSAS} on a 7-point scale.}
% \label{fig:questionnaires}
% \end{figure}

\section{Discussion and Conclusion}\label{sec:discussion}
The experiment establishes an initial connection between social credibility and safety-related authority of social robots.
Although there is no statistical significance, there are several indications that a robot that violates social norms is negatively affected in its safety authority.
This becomes especially prominent with hazards that users do not perceive to be particularly dangerous.
%
It could be the case that participants liked the social robot more, wanted to interact with it, and therefore followed its instructions accordingly. Another reason for the observed effect could be that participants trusted \ac{AN}'s safety assessment over their own and as a result better responded to its alerts.

% \section{Conclusion}\label{sec:conclusion}
In future work, we will attempt to assess the effect of safety-critical and safety-related alerts on the rated sociability of the robot with a full fledged study, where current results provide size-effect calculations for the next study's sample-size needed to achieve a reasonable power.

%TODO for bib we can probably shorten it by using et al for 3+ authors
\bibliographystyle{splncs04}
\bibliography{bibliopraphy}

\end{document}
