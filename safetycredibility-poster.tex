\NeedsTeXFormat{LaTeX2e}
\documentclass[a0, portrait]{a0poster}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage[hidelinks]{hyperref}
\usepackage{latexsym}
\usepackage{amsmath, amssymb}
\usepackage{color}
\usepackage{mfirstuc}

\usepackage{geometry}
\usepackage{multicol}

\usepackage{vwcol}
\usepackage{tabularx}

\usepackage{authentic-poster}
\usepackage{acro}
\DeclareAcronym{RoSAS}{short = RoSAS, long = Robotic Social Attributes Scale}
\DeclareAcronym{fetch}{short = \emph{Fetch}, long = \emph{Fetch Mobile Manipulator}}
\DeclareAcronym{HRI}{short = HRI, long = Human-Robot Interaction}
\DeclareAcronym{AN}{short = \emph{AN}, long = \emph{According to social Norms}}
\DeclareAcronym{VN}{short = \emph{VN}, long = \emph{Violating social Norms}}
\DeclareAcronym{oven}{short = \emph{oven}, long = \emph{severe}}
\DeclareAcronym{plugs}{short = \emph{power plugs}, long = \emph{minor}}
\DeclareAcronym{pepper}{short = \emph{Pepper}, long = \emph{moderate}}

\pagestyle{empty}
\setlength{\parindent}{0pt}

\title{How a Robot's Social Credibility Affects Safety Performance}
\author{Patrick Holthaus, Catherine Menon, Farshid Amirabdollahian}
\institute{Adaptive Systems Research Group, School of Engineering and Computer Science, University of Hertfordshire}

\hypersetup{
  pdftitle={How a Robot's Social Credibility Affects Safety Performance},
  pdfauthor={Patrick Holthaus},
  pdfsubject={ICSR 2019},
  pdfkeywords={Human-Robot interaction, Social credibility, Robot safety.}
}

\graphicspath{{figures/}}

\setlength{\multicolsep}{0pt}

\begin{document}

\maketitle

\posterbox[\linewidth]{Motivation}{
  \setlength{\columnsep}{4ex}
  \setlength{\columnseprule}{2pt}
  \begin{multicols}{2}
    \begin{vwcol}[widths={0.75,0.25},rule=0pt]
    Establishing a link between a robot's social credibility and its authority regarding safety-related functions:
    \\[.5em]\noindent
    Is there a negative effect of a lack of social credibility on the willingness of humans to follow safety-related alerts and the thoroughness of their actions?

    \columnbreak

    \centering
     \includegraphics[width=.18\linewidth]{fetch}
    \end{vwcol}

    \columnbreak

    \textbf{Preliminary user study}

    A social robot alerts participants during a puzzle-solving task to a safety hazard.
    \\[.5em]
    \textbf{Independent variables}
    \begin{itemize}
     \item Robot behaviours: \ac{AN} vs \ac{VN}
     \item Severity of safety hazard: \ac{oven} vs \ac{plugs} vs \ac{pepper}
    \end{itemize}

  \end{multicols}
}

\begin{multicols}{2}

  \posterbox[\linewidth]{Robot behaviours}{
    \begin{tabularx}{\linewidth}{XcX}
        \Large \textbf{\textcolor{authentic@primary}{\acl{AN}}} & \textbf{vs} & \raggedleft\Large\textbf{\textcolor{authentic@tertiary}{\acl{VN}}}
    \end{tabularx}
    \begin{tabularx}{\linewidth}{XcX}
        appropriate & greeting distance & \raggedleft too far
    \end{tabularx}
    \begin{tabularx}{\linewidth}{XcX}
        appropriate & passing distance when monitoring & \raggedleft too close
    \end{tabularx}
    \begin{tabularx}{\linewidth}{XcX}
        frontal & position when interrupting & \raggedleft from behind
    \end{tabularx}
    \begin{tabularx}{\linewidth}{XcX}
        up & head posture when interrupting & \raggedleft down
    \end{tabularx}
    \begin{tabularx}{\linewidth}{XcX}
        polite & verbal interruptions and confirmations & \raggedleft impolite
    \end{tabularx}
  }
  \posterbox[\linewidth]{Experimental procedure}{
    {\bfseries Participant task: Complete as many Sudoku puzzles as possible!}
    \begin{itemize}
    \item Robot constantly inspects the experimentation area
    \item Robot interrupts participant 4x with a hazard warning
    \item Participant decides whether to react to warning or continue puzzle
    \end{itemize}
    {\bfseries Dependent variables}
    \begin{itemize}
        \item Assessment of hazard severity (semantic-differential questions)
        \item Social perception of robot (\emph{RoSAS}, \emph{GodSpeed} questionnaires)
        \item Reaction to hazard warning (manipulation times, response rates)
    \end{itemize}
  }%
  \posterbox[\linewidth]{Experimentation area}{
    \hspace*{28.7cm}\includegraphics[width=.25\linewidth]{uh-robothouse75-2}\\[.5em]
    \includegraphics[width=\linewidth, trim={0 2cm 0 12cm}, clip]{fetch-house}\\[.5em]
    \includegraphics[width=\linewidth]{robothouse-map}
     Participants are positioned at the green circle.
     Initial robot positions are indicated with blue squares.
     Brown crosses mark inspection areas for the robot.
     The \ac{oven} and switchable \ac{plugs} are located inside the kitchen.
     Robot paths are delineated if different between \ac{AN} and \ac{VN}.
     Details at \href{https://robothouse.herts.ac.uk}{robothouse.herts.ac.uk}
  }%

%   \columnbreak

  \posterbox[\linewidth]{Safety hazards}{
    {\Large \bfseries
        \hspace{4.75cm} \emph{Oven} \hspace{4.7cm} vs
        \hspace{2.05cm} \emph{Power plugs} \hspace{2.05cm} vs
        \hspace{.7cm} \emph{Pepper}
    }

    {\Large
        \hspace{4.7cm} left on \hspace{10.9cm} left on \hspace{5.3cm} overheating \\[.3em]
    }
    \includegraphics[height=.094\textheight]{kitchen2}
    \includegraphics[height=.094\textheight]{kitchen1}
    \includegraphics[height=.094\textheight]{pepper2}
  }
  \posterbox[\linewidth]{Hazard severity and manipulation times}{
    \setlength{\columnsep}{4ex}
    \setlength{\columnseprule}{0pt}
    \begin{multicols}{2}
    \includegraphics[width=.5\linewidth,page=1]{Rplots}%
    \includegraphics[width=.5\linewidth,page=12]{Rplots}%

    \columnbreak

    {\bfseries Hazard severity ratings}
    \begin{itemize}
     \item Distinct difference between \ac{plugs} and the others
    \end{itemize}

    {\bfseries Manipulation times}
    \begin{itemize}
     \item Participants in \ac{VN} took longer to off \ac{plugs}
     \item Timings for \ac{pepper} similar
     \item Difference in variance for \ac{plugs} and \ac{pepper}
    \end{itemize}
  \end{multicols}
  }
  \posterbox[\linewidth]{Reaction to hazard warnings}{
    \includegraphics[width=.25\linewidth,page=8]{Rplots}%
    \includegraphics[width=.25\linewidth,page=9]{Rplots}%
    \includegraphics[width=.25\linewidth,page=10]{Rplots}%
    \includegraphics[width=.25\linewidth,page=11]{Rplots}%

    {\bfseries Response and manipulation rates}
    \begin{itemize}
     \item Better hazard response (R) and target manipulation (M) in \ac{AN}
     \item Reaction rates decrease between the two \ac{plugs} in \ac{VN}
     \item Target manipulation differs from response rate in \ac{VN}
    \end{itemize}
  }

\posterbox[\linewidth]{Discussion}{
  \setlength{\columnsep}{4ex}
  \setlength{\columnseprule}{2pt}
% \begin{multicols}{2}
  {\bfseries Interpretation of results}
  \begin{itemize}
%     \item Initial link between social credibility and safety-related authority
    \item Violation of social norms might negatively affect safety authority
    \item Participants might have trusted the robot's safety assessment in \ac{AN}
    \item Seemingly lesser effect with higher perceived hazard severity
  \end{itemize}
%   \columnbreak
  {\bfseries Limitations and future work}
  \begin{itemize}
    \item No significant observations (only 30 participants)
    \item Participants might have reacted because they like the robot in \ac{AN}
    \item Full sample size study with a more obviously social robot planned
  \end{itemize}
}

\end{multicols}%

\vfill
\makefooter{%
Contact: \href{mailto:p.holthaus@herts.ac.uk}{p.holthaus@herts.ac.uk} \hspace*{\fill} Group website: \href{http://adapsys.cs.herts.ac.uk}{adapsys.cs.herts.ac.uk} \hfill Sources available: \href{https://gitlab.com/robothouse/rh-experiments/soccred}{gitlab.com/robothouse/rh-experiments/soccred}%
}
\end{document}
