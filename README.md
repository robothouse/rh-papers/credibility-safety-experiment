# How a Robot's Social Credibility Affects Safety Performance

A paper at the International Conference on Social Robotics (ICSR) 2019.

[Latest Paper for ICSR 2019](https://gitlab.com/robothouse/rh-papers/credibility-safety-experiment/-/jobs/artifacts/master/raw/safetycredibility-paper.pdf?job=PDFs)

[Latest Poster for ICSR 2019](https://gitlab.com/robothouse/rh-papers/credibility-safety-experiment/-/jobs/artifacts/master/raw/safetycredibility-poster.pdf?job=PDFs)

Sources for the experimental procedure [are available](https://gitlab.com/robothouse/rh-experiments/soccred).